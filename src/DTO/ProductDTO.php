<?php

namespace VmdCms\Modules\Products\DTO;

use Illuminate\Support\Collection;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;
use VmdCms\Modules\Products\Contracts\ProductDTOInterface;
use VmdCms\Modules\Products\Models\Product;

class ProductDTO implements ProductDTOInterface
{
    protected $product;

    protected $id;

    protected $statuses;
    protected $status;
    protected $statusSlug;
    protected $statusTitle;

    protected $isFavorite;

    protected $imagePath;
    protected $title;
    protected $url;
    protected $code;

    protected $stockTitle;
    protected $supplierTitle;

    protected $itemsInBox;

    protected $catalogs;
    protected $catalog;
    protected $catalogTitle;
    protected $catalogUrl;

    /**
     * @var Collection
     */
    protected $prices;

    /**
     * @var array
     */
    protected $pricesFiltersGroups;
    protected $photos;

    /**
     * @var \VmdCms\Modules\Prices\DTO\PriceDTO
     */
    protected $minPriceDTO;

    public function __construct(Product $product, bool $withPrice = false)
    {
        $this->product = $product;

        $this->id = $product->id;
        $this->isFavorite = $product->isFavorite;
        $this->statuses = $product->status ?? null;
        $this->status = $this->statuses ? $this->statuses->first() : null;
        $this->statusTitle = $this->status ? $this->status->info->title ?? null : null;
        $this->statusSlug = $this->status ? $this->status->slug ?? null : null;

        $this->imagePath = $product->imagePath ?? null;
        $this->title = $product->info->title ?? null;
        $this->url = $product->urlPath ?? null;
        $this->code = $product->code ?? null;

        $this->stockTitle = $product->availability ? $product->availability->info->title ?? null : null;
        $this->supplierTitle = $product->supplier->info->title ?? null;

        $this->itemsInBox = $product->settings ? $product->settings->items_in_box ?? 0 : null;

        $this->catalogs = $product->catalog ?? null;
        $this->catalog = $this->catalogs ? $this->catalogs->first() : null;
        $this->catalogTitle = $this->catalog ? $this->catalog->info->title ?? null : null;
        $this->catalogUrl = $this->catalog ? $this->catalog->urlPath ?? null : null;

        if($withPrice){
            $this->setPrices($product);
        }

    }

    protected function setPrices(Product $product)
    {
        if(!class_exists(\VmdCms\Modules\Prices\Models\Price::class)){
            throw new ModuleNotActiveException();
        }
        $prices = $product->prices ?? null;
        $minPriceDTO = null;
        $pricesCollection = new \VmdCms\Modules\Prices\Collections\PriceDTOCollection();
        $priceFilterGroups = [];
        if(is_countable($prices) && count($prices))
        {
            foreach ($prices as $price)
            {
                $priceDto = new \VmdCms\Modules\Prices\DTO\PriceDTO($price,true, $this);
                if(!$minPriceDTO instanceof \VmdCms\Modules\Prices\DTO\PriceDTO || $minPriceDTO->getPriceBase() > $priceDto->getPriceBase())
                {

                    $minPriceDTO = $priceDto;
                }

                foreach ($priceDto->getFiltersDTOCollection()->getItems() as $group)
                {
                    if(isset($priceFilterGroups[$group->id])) {
                        foreach ($group->getChildren()->getItems() as $child)
                        {
                            if($priceFilterGroups[$group->id]->getChildren()->getItems()->where('id',$child->id)->first()) continue;
                            $priceFilterGroups[$group->id]->getChildren()->append($child);
                        }
                    }
                    else{
                        $priceFilterGroups[$group->id] = unserialize(serialize($group));
                    }
                }

                $pricesCollection->append($priceDto);
            }
        }
        $this->prices = $pricesCollection->getItems();
        $this->minPriceDTO = $minPriceDTO;
        $this->pricesFiltersGroups = $priceFilterGroups;
    }

    public function setStatus(\VmdCms\Modules\Taxonomies\Models\Status $status){
        $this->status = $status;
        $this->statusTitle = $status->info->title ?? null;
        $this->statusSlug = $status->slug ?? null;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isFavorite(): bool
    {
        return $this->isFavorite;
    }

    /**
     * @return Collection|null
     */
    public function getStatuses(): ?Collection
    {
        return $this->statuses;
    }

    /**
     * @return CmsModelInterface|null
     */
    public function getStatus(): ?CmsModelInterface
    {
        return $this->status;
    }

    /**
     * @return string|null
     */
    public function getStatusTitle(): ?string
    {
        return $this->statusTitle;
    }

    /**
     * @return string|null
     */
    public function getStatusSlug(): ?string
    {
        return $this->statusSlug;
    }

    /**
     * @return string|null
     */
    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return string|null
     */
    public function getStockTitle(): ?string
    {
        return $this->stockTitle;
    }

    /**
     * @return string|null
     */
    public function getSupplierTitle(): ?string
    {
        return $this->supplierTitle;
    }

    /**
     * @return int|null
     */
    public function getItemsInBox(): ?int
    {
        return $this->itemsInBox;
    }

    /**
     * @return Collection|null
     */
    public function getCatalogs(): ?Collection
    {
        return $this->catalogs;
    }

    /**
     * @return CmsModelInterface|null
     */
    public function getCatalog(): ?CmsModelInterface
    {
        return $this->catalog;
    }

    /**
     * @return string|null
     */
    public function getCatalogTitle(): ?string
    {
        return $this->catalogTitle;
    }

    /**
     * @return string|null
     */
    public function getCatalogUrl(): ?string
    {
        return $this->catalogUrl;
    }

    /**
     * @return Collection
     */
    public function getPrices(): Collection
    {
        return $this->prices;
    }

    /**
     * @return null|\VmdCms\Modules\Prices\DTO\PriceDTO
     */
    public function getMinPriceDTO(): ?\VmdCms\Modules\Prices\DTO\PriceDTO
    {
        return $this->minPriceDTO;
    }

    /**
     * @return array|null
     */
    public function getPricesFiltersGroups(): ?array
    {
        return $this->pricesFiltersGroups;
    }

    /**
     * @param int $groupId
     * @return null|\VmdCms\Modules\Taxonomies\DTO\TaxonomyDTO
     */
    public function getPricesFiltersGroup(int $groupId): ?\VmdCms\Modules\Taxonomies\DTO\TaxonomyDTO
    {
        return $this->pricesFiltersGroups[$groupId] ?? null;
    }

    /**
     * @return array
     */
    public function getProductPhotos(): array
    {
        if(empty($this->photos)){
            $this->photos = [];

            $photos = $this->product->photos ?? null;
            if(is_countable($photos) && count($photos)) {
                foreach ($photos as $photo)
                {
                    if(!$photo->active) continue;
                    $this->photos[] = [
                        'path' => $photo->imagePath,
                        'title' => $photo->title
                    ];
                }
            }
            elseif(!empty($this->imagePath)){
                $this->photos[] = [
                    'path' => $this->imagePath,
                    'title' => $this->title
                ];
            }
        }

        return $this->photos;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->product->info->description ?? null;
    }

    protected function getStatusesArray()
    {
        $statuses = [];
        if(is_countable($this->statuses) && count($this->statuses))
        {
            foreach ($this->statuses as $status)
            {
                $statuses[] = [
                    'id' => $status->id,
                    'title' => $status->info->title ?? null
                ];
            }
        }
        return $statuses;
    }

    protected function getCatalogsArray()
    {
        $catalogs = [];
        if(is_countable($this->catalogs) && count($this->catalogs))
        {
            foreach ($this->catalogs as $catalog)
            {
                $catalogs[] = [
                    'id' => $catalog->id,
                    'title' => $catalog->info->title ?? null,
                    'url' => $catalog->urlPath ?? null,
                ];
            }
        }
        return $catalogs;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'statuses' => $this->getStatusesArray(),
            'statusTitle' => $this->statusTitle,
            'isFavorite' => $this->isFavorite,
            'imagePath' => $this->imagePath,
            'title' => $this->title,
            'url' => $this->url,
            'stockTitle' => $this->stockTitle,
            'supplierTitle' => $this->supplierTitle,
            'catalogs' => $this->getCatalogsArray(),
            'catalogTitle' => $this->catalogTitle,
            'catalogUrl' => $this->catalogUrl,
        ];
    }
}
