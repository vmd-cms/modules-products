<?php

namespace VmdCms\Modules\Products\Controllers;

use App\Modules\Products\Entity\ProductEntity;
use Illuminate\Support\Facades\Response;
use VmdCms\CoreCms\Controllers\CoreController;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;

class ProductsController extends CoreController
{
    public function setFavorite(int $id)
    {
        if(!class_exists(\VmdCms\Modules\Users\Models\User::class)){
            throw new ModuleNotActiveException();
        }
        $user = auth()->guard('user')->user();
        if(!$user instanceof \VmdCms\Modules\Users\Models\User) abort(403);

        if(!is_int($id)) abort(409);

        return Response::json([
            'success' => (new ProductEntity())->setFavorite($user->id,$id)
        ]);
    }
}
