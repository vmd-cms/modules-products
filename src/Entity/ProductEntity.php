<?php

namespace VmdCms\Modules\Products\Entity;

use App\Modules\Content\Services\DataShare;
use VmdCms\Modules\Products\DTO\ProductDTO;
use VmdCms\Modules\Products\Models\Product;
use VmdCms\Modules\Products\Models\ProductWatched;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;
use VmdCms\CoreCms\Services\Auth\UserSession;
use VmdCms\Modules\Products\Collections\ProductDTOCollection;
use VmdCms\Modules\Products\Contracts\ProductDTOCollectionInterface;
use VmdCms\Modules\Products\Models\ProductFavorite;

class ProductEntity
{
    /**
     * @param int $userId
     * @param int $productId
     * @return bool
     */
    public function setFavorite(int $userId,int $productId)
    {
        try {
            $favorite = ProductFavorite::where('user_id',$userId)
                ->where('products_id',$productId)
                ->first();
            if($favorite){
                $favorite->delete();
            }
            else {
                $favorite = new ProductFavorite();
                $favorite->products_id = $productId;
                $favorite->user_id = $userId;
                $favorite->save();
            }
        }
        catch (\Exception $exception)
        {
            return false;
        }
        return true;
    }

    /**
     * @param Product $product
     * @throws ModuleNotActiveException
     */
    public function setProductWatched(Product $product){

        if(!class_exists(\VmdCms\Modules\Users\Entity\Auth\AuthEntity::class)){
            throw new ModuleNotActiveException();
        }

        try {
            $sessionId = UserSession::getUserSessionId();
            if($userId = \VmdCms\Modules\Users\Entity\Auth\AuthEntity::getAuthId()){
                DB::table(ProductWatched::table())->where('session_id',$sessionId)->update([
                    'user_id' => $userId
                ]);
            }
            ProductWatched::updateOrCreate([
                'session_id' => $sessionId,
                'user_id'    => $userId,
                'products_id'=> $product->id,
            ],[
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
        catch (\Exception $exception){

        }
    }

    /**
     * @param string $url
     * @throws ModelNotFoundException
     * @throws ModuleNotActiveException
     */
    public function shareProductData(string $url)
    {
        if(!class_exists(\VmdCms\Modules\Prices\Models\Price::class)){
            throw new ModuleNotActiveException();
        }

        $product = (new Product())->getModelByUrl($url);
        if(!$product instanceof Product) throw new ModelNotFoundException();

        $prices = \VmdCms\Modules\Prices\Models\Price::limit(5)->get();
        $collection = new \VmdCms\Modules\Prices\Collections\PriceDTOCollection();
        foreach ($prices as $item)
        {
            $collection->append(new \VmdCms\Modules\Prices\DTO\PriceDTO($item));
        }

        DataShare::getInstance()->appendData('priceProductItems',$collection->getItems());

        DataShare::getInstance()->appendData('product',$product);
    }

    /**
     * @param int $limit
     * @return ProductDTOCollectionInterface
     * @throws ModuleNotActiveException
     */
    public function getWatchedItems(int $limit = 5): ProductDTOCollectionInterface
    {
        if(!class_exists(\VmdCms\Modules\Users\Entity\Auth\AuthEntity::class)){
            throw new ModuleNotActiveException();
        }

        $query = ProductWatched::query();
        if($userId = \VmdCms\Modules\Users\Entity\Auth\AuthEntity::getAuthId()){
            $query->where('user_id',$userId);
        }
        else{
            $query->where('session_id',UserSession::getUserSessionId());
        }

        $productDTO = DataShare::getInstance()->productDTO ?? null;
        if($productDTO instanceof \VmdCms\Modules\Products\DTO\ProductDTO)
        {
            $query->where('products_id','!=',$productDTO->getId());
        }

        $watched = $query->whereHas('product',function ($q){
            $q->active();
        })->orderBy('updated_at','desc')->with('product')->limit($limit)->get();

        $dtoCollection = new ProductDTOCollection();
        if(is_countable($watched) && count($watched)){
            foreach ($watched as $item)
            {
                if(!$item->product instanceof Product) continue;

                $dtoCollection->append(new ProductDTO($item->product,true));
            }
        }
        return $dtoCollection;
    }

    /**
     * @param Collection|null $items
     * @return ProductDTOCollectionInterface
     */
    protected function getDtoCollection(Collection $items = null): ProductDTOCollectionInterface
    {
        $dtoCollection = new ProductDTOCollection();
        if(is_countable($items) && count($items)){
            foreach ($items as $item)
            {
                if(!$item instanceof Product) continue;

                $dtoCollection->append(new ProductDTO($item));
            }
        }
        return $dtoCollection;
    }
}
