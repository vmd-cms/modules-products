<?php

namespace VmdCms\Modules\Products\Entity;

use App\Modules\Content\Services\DataShare;
use App\Modules\Products\DTO\ProductDTO;
use App\Modules\Products\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;
use VmdCms\Modules\Products\Collections\ProductDTOCollection;

class ProductCatalogEntity
{
    const LIMIT = 20;

    /**
     * @var array
     */
    protected $taxonomyGroups;

    protected $minPrice;

    protected $maxPrice;

    protected $sortType;

    protected $needle;

    protected $limit;

    public function __construct()
    {
        $this->limit = static::LIMIT;
    }

    public function setNeedle(string $needle = null) : self
    {
        $this->needle = $needle;
        return $this;
    }

    public function setSortType(string $sortType) : self
    {
        $this->sortType = $sortType;
        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }


    /**
     * @param array $taxonomyGroups
     * @return $this
     */
    public function setTaxonomyGroups(array $taxonomyGroups): self
    {
        $this->taxonomyGroups = $taxonomyGroups;
        return $this;
    }

    /**
     * @param null|float $minPrice
     * @return $this
     */
    public function setMinPrice(?float $minPrice = 0): self
    {
        $this->minPrice = $minPrice;
        return $this;
    }

    /**
     * @param null|float $minPrice
     * @return $this
     */
    public function setMaxPrice(?float $maxPrice = 0): self
    {
        $this->maxPrice = $maxPrice;
        return $this;
    }

    /**
     * @param array $catalogIds
     * @return array
     */
    public function getTaxonomiesCount(array $catalogIds)
    {
        $filterCounts = $this->getProductsQuery($catalogIds)->select(DB::raw("pr_tax.taxonomies_id, COUNT(DISTINCT pr_tax.products_id) as total"))
            ->join('products_taxonomies as pr_tax','pr_tax.products_id','=','products.id')
            ->groupBy('pr_tax.taxonomies_id')
            ->get();

        $countedTaxonomies = [];

        if(is_countable($filterCounts) && count($filterCounts)){
            foreach ($filterCounts as $item)
            {
                $countedTaxonomies[$item->taxonomies_id] = $item->total;
            }
        }

        return $countedTaxonomies;
    }

    /**
     * @param array $catalogIds
     * @param callable|null $query
     * @return Collection
     */
    public function getProductIdsArr(array $catalogIds,callable $query = null)
    {
        $productIds = $this->getProductsQuery($catalogIds)->select('products.id');
        if(is_callable($query)) call_user_func($query,$productIds);
        $this->appendSortConditions($productIds);
        $productIds = $productIds->groupBy('products.id')->get();
        $arrIds = $productIds->map(function ($item){
            return $item->id;
        });
        return $arrIds;
    }

    /**
     * @param array $catalogIds
     * @param int $page
     * @return mixed
     */
    public function getProductsByCatalogId(array $catalogIds, $page = 1)
    {
        $arrIds = $this->getProductIdsArr($catalogIds, function ($q) use ($page){
            if($page > 1) $q->skip(($page-1) * $this->limit);
            $q->limit($this->limit);
        });

        $builder = Product::select(DB::raw('products.*, count(*) as `aggregate`'))
            ->whereIn('products.id',$arrIds)->with(['info','prices']);

        $this->appendSortConditions($builder);

        $builder->with(['info','favorite','status.info','availability.info','supplier.info','settings','catalog.info','prices.filters']);
        return $builder->groupBy('products.id')->get();
    }

    /**
     * @param array $catalogIds
     * @param int $page
     * @return ProductDTOCollection
     */
    public function getProductsDtoCollectionByCatalogId(array $catalogIds,int $page = 1)
    {
        $products = $this->getProductsByCatalogId($catalogIds,$page);
        $productsCollection = new ProductDTOCollection();
        foreach ($products as $product)
        {
            $productsCollection->append(new ProductDTO($product));
        }
        return $productsCollection;
    }

    /**
     * @param array $catalogIds
     * @return int
     */
    public function getCountProductsInCatalog(array $catalogIds)
    {
        return $this->getProductsQuery($catalogIds)->distinct('products.id')->count();
    }

    /**
     * @param int $total
     * @return string
     */
    public function getTotalStr(int $total) : string
    {
        if($total == 1)
        {
            $totalStr = DataShare::getInstance()->translates->getValue('product_single');
        }
        elseif ($total > 1 && $total < 5)
        {
            $totalStr = DataShare::getInstance()->translates->getValue('product_');
        }
        else{
            $totalStr = DataShare::getInstance()->translates->getValue('product_plural');
        }
        return $total . ' ' . $totalStr;
    }

    /**
     * @param int $total
     * @param int $page
     * @return string
     */
    public function getTotalBlockStr(int $total, int $page): string
    {
        $countStart = $page == 1 ? 1 : ($page-1) * $this->limit + 1;
        $countFinish = $page * $this->limit < $total ? $page * $this->limit : $total;
        return DataShare::getInstance()->getTranslate('products') . ' ' . $countStart . '-' . $countFinish . ' ' . DataShare::getInstance()->getTranslate('from') . ' ' . $total;
    }

    public function getPricesRange(array $catalogIds){
        $query = $this->getProductsQuery($catalogIds,false);
        $range = $query->select([
            DB::raw("MAX(prices.price) as max_price"),
            DB::raw("MIN(prices.price) as min_price"),
        ])
            ->join('prices','prices.products_id','=','products.id')
            ->where('prices.active',true)
            ->where('prices.quantity','>=',1)
            ->first();
        return [
            'min' => intval($range->min_price),
            'max' => intval($range->max_price)
        ];
    }


    /**
     * @param array $catalogIds
     * @param bool $withPriceRange
     * @return \Illuminate\Database\Query\Builder
     */
    protected function getProductsQuery(array $catalogIds, $withPriceRange = true)
    {
        $query = DB::table('products')->where('products.active',true);

        if(count($catalogIds)){
            $query->join('products_catalogs','products_catalogs.products_id','=','products.id')
                ->whereIn('products_catalogs.catalogs_id',$catalogIds);
        }

        if(!empty($this->needle)){
            $query->join('products_info','products_info.products_id','=','products.id')->where(function ($q){
                $q->where('products_info.title','like','%' . $this->needle .'%')
                    ->orWhere('products_info.description_short','like','%' . $this->needle .'%');
            });
        }
        if($withPriceRange){
            $this->appendPriceRangeConditions($query);
        }
        $this->appendTaxonomiesConditions($query);
        return $query;
    }

    /**
     * @param \Illuminate\Database\Query\Builder $builder
     */
    protected function appendPriceRangeConditions(\Illuminate\Database\Query\Builder $builder){

        if($this->minPrice || $this->maxPrice) {
            $builder->whereIn('products.id', function ($q) {
                $q->select('products_id')->from('prices');
                $q->groupBy('products_id');
                if($this->minPrice) $q->havingRaw("MIN(price)>=$this->minPrice");
                if($this->maxPrice) $q->havingRaw("MIN(price)<=$this->maxPrice");
            });
        }
        return;
    }

    /**
     * @param \Illuminate\Database\Query\Builder $builder
     */
    protected function appendTaxonomiesConditions(\Illuminate\Database\Query\Builder $builder){

        if(!is_countable($this->taxonomyGroups) || !count($this->taxonomyGroups)) return;

        foreach ($this->taxonomyGroups as $group=>$items)
        {
            $builder->join("products_taxonomies as $group","$group.products_id",'=','products.id')
                ->whereIn("$group.taxonomies_id",$items);
        }
        return;
    }


    protected function appendSortConditions($builder){

        if(!class_exists(\VmdCms\Modules\Catalogs\Services\SortEnum::class)){
            throw new ModuleNotActiveException();
        }
        if(!$this->sortType) return;

        if($this->sortType === \VmdCms\Modules\Catalogs\Services\SortEnum::PRICE_ASC)
        {
            $builder->join("prices","prices.products_id",'=','products.id')
                ->orderByRaw("MIN(prices.price) ASC");
        }
        elseif($this->sortType === \VmdCms\Modules\Catalogs\Services\SortEnum::PRICE_DESC)
        {
            $builder->join("prices","prices.products_id",'=','products.id')
                ->orderByRaw("MIN(prices.price) DESC");
        }
        return;
    }

    /**
     * @param int $catalogId
     * @return mixed
     */
    public function getCatalogTreeIds(int $catalogId): array
    {
        if(!class_exists(\VmdCms\Modules\Catalogs\Models\Catalog::class)){
            throw new ModuleNotActiveException();
        }

        $catalogIds = [
            $catalogId => $catalogId
        ];
        $result =  DB::table(\VmdCms\Modules\Catalogs\Models\Catalog::table() . ' as l1')->select(['l2.id as id_l2','l3.id as id_l3','l4.id as id_l4'])
            ->leftJoin(\VmdCms\Modules\Catalogs\Models\Catalog::table() . ' as l2','l1.id','=','l2.parent_id')
            ->leftJoin(\VmdCms\Modules\Catalogs\Models\Catalog::table() . ' as l3','l2.id','=','l3.parent_id')
            ->leftJoin(\VmdCms\Modules\Catalogs\Models\Catalog::table() . ' as l4','l3.id','=','l4.parent_id')
            ->where('l1.id',$catalogId)
            ->get();

        if(is_countable($result) && count($result)){
            for ($i = 0; $i < $result->count(); $i ++){
                if(isset($result[$i]->id_l2)) $catalogIds[$result[$i]->id_l2] = $result[$i]->id_l2;
                if(isset($result[$i]->id_l3)) $catalogIds[$result[$i]->id_l3] = $result[$i]->id_l3;
                if(isset($result[$i]->id_l4)) $catalogIds[$result[$i]->id_l4] = $result[$i]->id_l4;
            }
        }

        return array_keys($catalogIds);
    }

    public function getProductsByCatalogs($catalog, int $limit = 5){

        if(!class_exists(\VmdCms\Modules\Catalogs\Models\Catalog::class)){
            throw new ModuleNotActiveException();
        }

        if(!$catalog instanceof \VmdCms\Modules\Catalogs\Models\Catalog){
            throw new CoreException();
        }

        $catalogIds = $this->getCatalogTreeIds($catalog->id);
        $this->setLimit($limit);
        return $this->getProductsByCatalogId($catalogIds);
    }
}
