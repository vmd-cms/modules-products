<?php

namespace VmdCms\Modules\Products\Initializers;

use VmdCms\CoreCms\CoreModules\Events\DTO\CoreEventModelDTO;
use VmdCms\CoreCms\CoreModules\Events\Entity\CoreEventsSetup;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventTypeEnums;
use VmdCms\CoreCms\Initializers\AbstractModuleInitializer;
use VmdCms\Modules\Products\Services\CoreEventEnums;

class ModuleInitializer extends AbstractModuleInitializer
{
    const SLUG = 'products';
    const ALIAS = 'Products';

    public function __construct()
    {
        parent::__construct();
        $this->stubBuilder->setPublishServices(true);
        $this->stubBuilder->setPublishDTO(true);
        $this->stubBuilder->setPublishEntity(true);
    }

    /**
     * @inheritDoc
     */
    public static function moduleAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @inheritDoc
     */
    public static function moduleSlug(): string
    {
        return self::SLUG;
    }

    public function seedCoreEvents()
    {
        CoreEventsSetup::getInstance()
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_PRODUCT_VIEW))
                ->setType(CoreEventTypeEnums::NOTIFICATION))
            ->seed();
    }
}
