<?php

namespace VmdCms\Modules\Products\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\Modules\Products\Models\Product;

interface ProductDTOInterface extends Arrayable
{
    /**
     * ProductDTOInterface constructor.
     * @param Product $product
     */
    public function __construct(Product $product);

    /**
     * @return Collection|null
     */
    public function getStatuses(): ?Collection;

    /**
     * @return CmsModelInterface|null
     */
    public function getStatus(): ?CmsModelInterface;

    /**
     * @return string|null
     */
    public function getStatusTitle(): ?string;

    /**
     * @return string|null
     */
    public function getImagePath(): ?string;

    /**
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * @return string|null
     */
    public function getUrl(): ?string;

    /**
     * @return string|null
     */
    public function getStockTitle(): ?string;

    /**
     * @return string|null
     */
    public function getSupplierTitle(): ?string;

    /**
     * @return Collection|null
     */
    public function getCatalogs(): ?Collection;

    /**
     * @return CmsModelInterface|null
     */
    public function getCatalog(): ?CmsModelInterface;

    /**
     * @return string|null
     */
    public function getCatalogTitle(): ?string;
    /**
     * @return string|null
     */
    public function getCatalogUrl(): ?string;
}
