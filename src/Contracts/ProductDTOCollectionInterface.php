<?php

namespace VmdCms\Modules\Products\Contracts;

use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

interface ProductDTOCollectionInterface extends CollectionInterface
{
    /**
     * @param ProductDTOInterface $productEntity
     */
    public function append(ProductDTOInterface $productEntity);

}
