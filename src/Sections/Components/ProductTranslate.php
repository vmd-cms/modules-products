<?php

namespace VmdCms\Modules\Products\Sections\Components;

use VmdCms\CoreCms\CoreModules\Translates\Sections\AbstractTranslate;

class ProductTranslate extends AbstractTranslate
{
    /**
     * @var string
     */
    protected $slug = 'product_translates';

    public function getCmsModelClass(): string
    {
        return \App\Modules\Products\Models\Components\ProductTranslate::class;
    }
}
