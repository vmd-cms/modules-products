<?php

namespace VmdCms\Modules\Products\Sections;

use VmdCms\CoreCms\Collections\FormTabsCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterComponent;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterDisplay;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterTreeComponent;
use VmdCms\CoreCms\Dashboard\Forms\Components\FormTab;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Facades\FormTableColumn;
use VmdCms\CoreCms\Models\CmsSection;

class Product extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'products';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('products');
    }

    public function display()
    {
        $filter = (new FilterDisplay());

        if(class_exists(\VmdCms\Modules\Taxonomies\Models\Taxonomy::class)){
            $filter->appendFilterComponent((new FilterComponent('info.title'))
                ->setFilterQueryCallback(function ($query,$value){
                    if(empty($value)) return;
                    $query->whereHas('status',function ($q) use ($value){
                        $q->where('taxonomies.id',$value);
                    });
                })
                ->setCountCallback(function ($model){
                    return \VmdCms\Modules\Products\Models\ProductTaxonomy::where('taxonomies_id',$model->id)->count();
                })
                ->setChipColorCallback(function ($model){
                    return $model->admin_color;
                })
                ->setIsExpansion(true)
                ->setHeadTitle(CoreLang::get('statuses'))
                ->setExpansionTitle(CoreLang::get('all'))
                ->setFilterSlug('product_status_param')
                ->setModelClass(\VmdCms\Modules\Taxonomies\Models\Status::class)
            );
        }

        if(class_exists(\VmdCms\Modules\Catalogs\Entity\CatalogEntity::class)){
            $filter->appendFilterComponent((new FilterTreeComponent('info.title'))
                ->setFilterQueryCallback(function ($query,$value){
                    if(empty($value)) return;

                    $catalogIds = (new \VmdCms\Modules\Catalogs\Entity\CatalogEntity())->getCatalogTreeIdsArr(intval($value));
                    $query->whereHas('catalog',function ($q) use ($catalogIds){
                        $q->whereIn('catalogs.id',$catalogIds);
                    });
                })
                ->setIsExpansion(true)
                ->setHeadTitle(CoreLang::get('catalog'))
                ->setExpansionTitle(CoreLang::get('all') . ' ' . CoreLang::get('catalogs'))
                ->setFilterSlug('product_catalog_param')
                ->setModelClass(\VmdCms\Modules\Catalogs\Models\Catalog::class)
            );
        }

        return Display::dataTable([
            Column::text('id','#')
                ->sortable()->searchable(),
            Column::photo('photo',CoreLang::get('photo'))->setPathCallback(function ($model){
                return $model->imagePath;
            }),
            Column::text('info.title',CoreLang::get('title'))
                ->setWidth('200')
                ->setSearchableCallback(function ($query,$search){
                    $query->orWhereHas('info',function ($q) use ($search){
                        $q->where('title','like','%'.$search.'%');
                    });
                })
                ->setSortableCallback(function ($query, $sortType) {
                    $query->join('products_info', function ($join) {
                        $join->on('products.id', '=', 'products_info.products_id');
                    });
                    $query->orderBy('products_info.title', $sortType)
                        ->selectRaw('products.*');
                }),
            Column::text('url','URL')
                ->sortable()->searchable(),
            Column::text('code','Артикул')
                ->sortable()->searchable(),
            ColumnEditable::switch('active')
                ->alignCenter()
                ->sortable(),
            Column::date('created_at')->setFormat('Y-m-d')
                ->sortable(),
        ])->orderDefault(function ($query){
            $query->orderBy('created_at','desc');
        })->setSearchable(true)->setFilter($filter);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        $tabs = new FormTabsCollection();

        $mainTabItems = [
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::url('url','Url')->setDependedField('info.title')->unique(),
            FormComponent::input('code',CoreLang::get('article')),
            FormComponent::switch('active'),
        ];

        if(class_exists(\VmdCms\Modules\Taxonomies\Models\Taxonomy::class)){
            $mainTabItems = array_merge($mainTabItems,[
                FormComponent::checkbox('status')->setDisplayField('info.title'),
                FormComponent::select('brand')->setDisplayField('info.title'),
                FormComponent::select('supplier')->setDisplayField('info.title'),
            ]);
        }

        if(class_exists(\VmdCms\Modules\Catalogs\Models\Catalog::class)){
            $mainTabItems = array_merge($mainTabItems,[
                FormComponent::tree('catalog')->setDisplayField('info.title')
            ]);
        }

        $tabsItems = [
            new FormTab(CoreLang::get('main'), $mainTabItems),
            new FormTab(CoreLang::get('description'),[
                FormComponent::text('info.description_short',CoreLang::get('short_description'))->maxLength(1024),
                FormComponent::ckeditor('info.description',CoreLang::get('full_description')),
            ]),
            new FormTab(CoreLang::get('images'),[
                FormComponent::image('photo',CoreLang::get('image'))->setHelpText(CoreLang::get('recommended_proportions') . ' 5:4 (500px x 400px)'),
                FormComponent::datatable('photos',CoreLang::get('images'))->setComponents([
                    FormTableColumn::image('value',CoreLang::get('image'))->setHelpText(CoreLang::get('recommended_proportions') . ' 10:9 (500px х 450px)'),
                    FormTableColumn::input('info.title',CoreLang::get('title')),
                    FormTableColumn::switch('active')->setDefault(true),
                ])->addClass('pt-5'),
                FormComponent::datatable('videos',CoreLang::get('video'))->setComponents([
                    FormTableColumn::image('value',CoreLang::get('video'))->setHelpText('test'),
                    FormTableColumn::input('info.title',CoreLang::get('title')),
                    FormTableColumn::switch('active'),
                ])->addClass('pt-10'),
                FormComponent::youtubeCode('youtubeCode.value', 'Youtube ' . CoreLang::get('code'))->addClass('pt-5'),
            ]),
        ];

        if(class_exists(\VmdCms\Modules\Taxonomies\Models\Taxonomy::class)){
            $tabsItems[] = new FormTab(CoreLang::get('filters'),[
                FormComponent::tree('filters','')->setDisplayField('info.title')
                    ->setAfterSaveCallback(function ($model){
                        if(request()->has('filters')){
                            $filterIds = explode(',',request()->get('filters',''));
                            (new \VmdCms\Modules\Prices\Entity\PriceEntity())->suspendProductPrices($model,$filterIds);
                        }
                    }),
            ]);
        }

        if(class_exists(\VmdCms\Modules\Prices\Models\Price::class)){
            $tabsItems[] = new FormTab(CoreLang::get('prices'),[
                FormComponent::custom('__pricePanel')
                    ->setCustomComponent((new \VmdCms\Modules\Prices\Dashboard\Forms\Components\Prices\ProductPricesPanelComponent('prices'))
                        ->setBottomHelpText(CoreLang::get('chose_price_depended'))
                        ->setComponents([
                            FormTableColumn::numeric('price',CoreLang::get('price'))->min(0)->setDefault(0)->setWidth(100),
                            FormTableColumn::numeric('discount_percent','%' . CoreLang::get('discount'))->min(0)->setDefault(0)->setWidth(100),
                            FormTableColumn::numeric('discount_price',CoreLang::get('discount_price'))->min(0)->setDefault(0)->setWidth(100),
                            FormTableColumn::numeric('quantity',CoreLang::get('quantity'))->min(0)->setDefault(0)->setWidth(100),
                            FormTableColumn::dateTime('discount_start',CoreLang::get('start_action'))->setWidth(250),
                            FormTableColumn::dateTime('discount_end',CoreLang::get('end_action'))->setWidth(250),
                            FormTableColumn::switch('active')->setWidth(100)->setDefault(true)->alignCenter()
                        ]))->addClass('pt-10'),
            ]);
        }

        $tabsItems[] = new FormTab('SEO',[
            FormComponent::seo('seo',''),
        ]);

        $tabs->appendItems($tabsItems);

        return Form::tabbed($tabs)->wide();
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Products\Models\Product::class;
    }

}
