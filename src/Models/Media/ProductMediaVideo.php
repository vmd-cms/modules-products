<?php

namespace VmdCms\Modules\Products\Models\Media;

class ProductMediaVideo extends ProductMedia
{
    public static function getModelKey()
    {
        return 'videos';
    }
}
