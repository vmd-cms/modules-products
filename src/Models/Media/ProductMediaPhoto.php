<?php

namespace VmdCms\Modules\Products\Models\Media;

use VmdCms\CoreCms\Contracts\Models\HasMediaDimensionsInterface;
use VmdCms\CoreCms\Traits\Models\HasImagePath;
use VmdCms\CoreCms\Traits\Models\HasMediaDimensions;

class ProductMediaPhoto extends ProductMedia implements HasMediaDimensionsInterface
{
    use HasImagePath, HasMediaDimensions;

    public static function getModelKey()
    {
        return 'photos';
    }

    /**
     * @return string|null
     */
    protected function getImageValue(): ?string
    {
        return $this->value ?? null;
    }
}
