<?php

namespace VmdCms\Modules\Products\Models\Media;

use VmdCms\CoreCms\Models\CmsInfoModel;

class ProductMediaInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'products_media_info';
    }

    /**
     * @return string
     */
    public static function getBaseModelClass(): string
    {
        return ProductMedia::class;
    }
}
