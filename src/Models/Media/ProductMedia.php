<?php

namespace VmdCms\Modules\Products\Models\Media;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\CmsRelatedModelInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\Modules\Products\Models\Product;

class ProductMedia extends CmsModel implements HasInfoInterface, CmsRelatedModelInterface
{
    use HasInfo;

    private function getInfoClass() : string
    {
        return ProductMediaInfo::class;
    }

    public static function table(): string
    {
        return 'products_media';
    }

    /**
     * @return string
     */
    public static function getForeignField() : string
    {
        return static::getBaseModelClass()::table() . '_id';
    }

    /**
     * @return string
     */
    public static function getBaseModelClass(): string
    {
        return Product::class;
    }

    public static function getModelKey(){
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getModelKey()){
            static::addGlobalScope('key', function (Builder $builder) {
                $builder->where('key', static::getModelKey());
            });
        }

        static::saving(function (ProductMedia $model){
            $model->key = static::getModelKey();
        });
    }
}
