<?php

namespace VmdCms\Modules\Products\Models\Media;

class ProductMediaYoutubeCode extends ProductMedia
{
    public static function getModelKey()
    {
        return 'youtube_code';
    }
}
