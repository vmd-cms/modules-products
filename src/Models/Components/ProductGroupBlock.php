<?php

namespace VmdCms\Modules\Products\Models\Components;

use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block;

class ProductGroupBlock extends Block
{
    public static function getModelGroup(): ?string
    {
        return 'product';
    }
}
