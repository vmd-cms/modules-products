<?php

namespace VmdCms\Modules\Products\Models\Components;

use VmdCms\CoreCms\CoreModules\Translates\Models\Translate;

class ProductTranslate extends Translate
{
    public static function getModelGroup(): ?string
    {
        return 'product';
    }
}
