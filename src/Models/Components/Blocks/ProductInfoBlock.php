<?php

namespace VmdCms\Modules\Products\Models\Components\Blocks;

use VmdCms\Modules\Products\Models\Components\ProductGroupBlock;
use VmdCms\Modules\Products\Services\BlockEnum;

class ProductInfoBlock extends ProductGroupBlock
{
    public static function getModelKey(): ?string
    {
        return BlockEnum::PRODUCT_INFO_BLOCK;
    }

    public function getJsonDataFieldsArr() : array
    {
        return ['title'];
    }
}
