<?php

namespace VmdCms\Modules\Products\Models;

use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;
use VmdCms\CoreCms\Models\CmsCrossModel;

class ProductCatalog extends CmsCrossModel
{
    public static function table(): string
    {
        return 'products_catalogs';
    }

    public function getBaseModelClass(): string
    {
        return Product::class;
    }

    public function getTargetModelClass(): string
    {
        if(!class_exists(\VmdCms\Modules\Catalogs\Models\Catalog::class)){
            throw new ModuleNotActiveException();
        }

        return \VmdCms\Modules\Catalogs\Models\Catalog::class;
    }
}
