<?php

namespace VmdCms\Modules\Products\Models;

use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;
use VmdCms\CoreCms\Models\CmsCrossModel;

class ProductTaxonomy extends CmsCrossModel
{
    public static function table(): string
    {
        return 'products_taxonomies';
    }

    public function getBaseModelClass(): string
    {
        return Product::class;
    }

    public function getTargetModelClass(): string
    {
        if(!class_exists(\VmdCms\Modules\Taxonomies\Models\Taxonomy::class)){
            throw new ModuleNotActiveException();
        }

        return \VmdCms\Modules\Taxonomies\Models\Taxonomy::class;
    }
}
