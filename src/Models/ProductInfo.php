<?php

namespace VmdCms\Modules\Products\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;
use VmdCms\CoreCms\Models\CmsModel;

class ProductInfo extends CmsInfoModel
{
    protected $fillable = ['products_id','title'];

    public static function table(): string
    {
        return 'products_info';
    }

    protected static function replicateData(CmsModel $model)
    {
        $model->url = $model->url . '-copy-' . date('Y-m-d-H:i:s');
    }
}
