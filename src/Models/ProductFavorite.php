<?php

namespace VmdCms\Modules\Products\Models;

use App\Modules\Products\Models\Product;
use VmdCms\CoreCms\Models\CmsModel;

class ProductFavorite extends CmsModel
{
    public static function table(): string
    {
        return 'products_favorites';
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'products_id','id');
    }
}
