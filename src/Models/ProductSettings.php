<?php

namespace VmdCms\Modules\Products\Models;

use VmdCms\CoreCms\Models\CmsModel;

class ProductSettings extends CmsModel
{
    public static function table(): string
    {
        return 'products_settings';
    }

    /**
     * @return string
     */
    public static function getForeignField() : string
    {
        return static::getBaseModelClass()::table() . '_id';
    }

    /**
     * @return string
     */
    public static function getBaseModelClass(): string
    {
        return Product::class;
    }
}
