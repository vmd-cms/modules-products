<?php

namespace VmdCms\Modules\Products\Models;

use App\Modules\Products\Models\ProductFavorite;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOne;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\CloneableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\HasMediaDimensionsInterface;
use VmdCms\CoreCms\Contracts\Models\SeoableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\Cloneable;
use VmdCms\CoreCms\Traits\Models\HasImagePath;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\HasMediaDimensions;
use VmdCms\CoreCms\Traits\Models\HasUrlPath;
use VmdCms\CoreCms\Traits\Models\JsonData;
use VmdCms\CoreCms\Traits\Models\Seoable;
use VmdCms\CoreCms\Traits\Related\HasCrossRelatedService;
use VmdCms\Modules\Products\Models\Media\ProductMediaPhoto;
use VmdCms\Modules\Products\Models\Media\ProductMediaVideo;
use VmdCms\Modules\Products\Models\Media\ProductMediaYoutubeCode;
use VmdCms\Modules\Products\Traits\Availability;
use VmdCms\Modules\Products\Traits\Brand;
use VmdCms\Modules\Products\Traits\Filters;
use VmdCms\Modules\Products\Traits\Status;
use VmdCms\Modules\Products\Traits\Supplier;
use VmdCms\Modules\Products\Traits\Prices;
use VmdCms\Modules\Products\Traits\Catalog;

class Product extends CmsModel implements ActivableInterface, HasInfoInterface, SeoableInterface, CloneableInterface, HasMediaDimensionsInterface
{
    use Activable, HasInfo, JsonData, Cloneable, HasImagePath, HasUrlPath, HasCrossRelatedService, Availability, Status, Supplier, Catalog, Brand, Filters, Seoable, Prices, HasMediaDimensions;

    public static function table(): string
    {
        return 'products';
    }

    protected $with = ['info'];

    /**
     * @return string
     */
    protected function getConfigModuleKey()
    {
        return 'products_module.prefix_single';
    }

    protected function getTaxonomyCrossModel(): string
    {
        return ProductTaxonomy::class;
    }

    protected function getCatalogCrossModel(): string
    {
        return ProductCatalog::class;
    }

    public static function getModelKey(){
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getModelKey()){
            static::addGlobalScope('key', function (Builder $builder) {
                $builder->where('products.key', static::getModelKey());
            });
            static::saving(function (Product $model){
                $model->key = static::getModelKey();
            });
        }
    }

    /**
     * @return HasOne
     */
    public function settings(): HasOne
    {
        return $this->hasOne(ProductSettings::class, 'products_id','id');
    }

    public function photos()
    {
        return $this->hasMany(ProductMediaPhoto::class,ProductMediaPhoto::getForeignField(),'id')->orderBy('order');
    }

    public function videos()
    {
        return $this->hasMany(ProductMediaVideo::class,ProductMediaVideo::getForeignField(),'id')->orderBy('order');
    }

    public function youtubeCode()
    {
        return $this->hasOne(ProductMediaYoutubeCode::class,ProductMediaYoutubeCode::getForeignField(),'id');
    }

    public function cloneableRelations(): array
    {
        return [
            'info','status','availability','settings','videos','youtubeCode','seo','prices','filters','catalog','brand'
        ];
    }

    public function getIsFavoriteAttribute()
    {
        return $this->favorite instanceof \VmdCms\Modules\Products\Models\ProductFavorite;
    }

    public function favorite()
    {
        if(!class_exists(\VmdCms\Modules\Products\Models\ProductFavorite::class)){
            return null;
        }
        return $this->hasOne(\VmdCms\Modules\Products\Models\ProductFavorite::class,'products_id','id')
            ->where('user_id',auth()->guard('user')->id());
    }
}
