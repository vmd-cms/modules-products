<?php

namespace VmdCms\Modules\Products\Models;

use App\Modules\Products\Models\Product;
use VmdCms\CoreCms\Models\CmsModel;

class ProductWatched extends CmsModel
{
    protected $fillable = [
        'session_id', 'user_id', 'products_id', 'updated_at'
    ];

    public static function table(): string
    {
        return 'products_watched';
    }

    public function product(){
        return $this->belongsTo(Product::class,'products_id','id');
    }
}
