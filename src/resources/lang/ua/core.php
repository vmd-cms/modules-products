<?php

return [
    /**
     * fields
     **/
    'order_statuses' => 'Статусы заказов',
    'all_statuses' => 'Все статусы',
    'statuses' => 'Статусы',
    'prices' => 'Цены',
    'discount' => 'Скидки',
    'discount_price' => 'Скидочная цена',
    'start_action' => 'Начало акции',
    'end_action' => 'Конец акции',

    /**
     * pages
     */
    'products' => 'Товары',

    /**
     * messages
     **/
    'chose_price_depended' => 'Выберете параметры при которых отображается данная цена:',

    /**
     * buttons
     **/

    /**
     * validates
     */
];
