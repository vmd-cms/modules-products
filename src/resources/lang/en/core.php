<?php

return [
    /**
     * fields
     **/
    'order_statuses' => 'Order Statuses',
    'all_statuses' => 'All Statuses',
    'statuses' => 'Statuses',
    'prices' => 'Prices',
    'discount' => 'Discount',
    'discount_price' => 'Discount Price',
    'start_action' => 'Start Action',
    'end_action' => 'End Action',

    /**
     * pages
     */
    'products' => 'Products',

    /**
     * messages
     **/
    'chose_price_depended' => 'Chose params which depends on price',

    /**
     * buttons
     **/

    /**
     * validates
     */
];
