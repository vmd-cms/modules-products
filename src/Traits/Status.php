<?php

namespace VmdCms\Modules\Products\Traits;

use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;

trait Status
{
    use TaxonomyRelated;

    /**
     * @param $value
     * @throws NotCmsModelException
     */
    public function setStatusAttribute($value)
    {
        $this->getCrossRelatedService()
            ->saveCrossRelation('status',$value,$this->getTaxonomyCrossModel());
    }

    /**
     * @return mixed
     * @throws ModuleNotActiveException
     * @throws NotCmsCrossModelException
     */
    public function status()
    {
        if(!class_exists(\VmdCms\Modules\Taxonomies\Models\Status::class)){
            throw new ModuleNotActiveException();
        }

        return  $this->getCrossRelatedService()
            ->setCrossModel($this->getTaxonomyCrossModel())
            ->setRelatedModel(\VmdCms\Modules\Taxonomies\Models\Status::class)
            ->hasThroughCrossModelWrapper('hasManyThrough');
    }

}
