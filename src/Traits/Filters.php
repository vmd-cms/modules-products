<?php

namespace VmdCms\Modules\Products\Traits;

use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;

trait Filters
{
    use TaxonomyRelated;

    /**
     * @param $value
     * @throws NotCmsModelException
     */
    public function setFiltersAttribute($value)
    {
        $this->getCrossRelatedService()
            ->saveCrossRelation('filters',$value, $this->getTaxonomyCrossModel());
    }

    /**
     * @return mixed
     * @throws ModuleNotActiveException
     * @throws NotCmsCrossModelException
     */
    public function filters()
    {
        if(!class_exists(\VmdCms\Modules\Taxonomies\Models\Filter::class)){
            throw new ModuleNotActiveException();
        }

        return  $this->getCrossRelatedService()
            ->setCrossModel($this->getTaxonomyCrossModel())
            ->setRelatedModel(\VmdCms\Modules\Taxonomies\Models\Filter::class)
            ->hasThroughCrossModelWrapper('hasManyThrough');
    }
}
