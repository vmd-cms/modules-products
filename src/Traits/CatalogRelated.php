<?php

namespace VmdCms\Modules\Products\Traits;

use VmdCms\CoreCms\Services\Related\CrossRelatedService;

trait CatalogRelated
{
    /**
     * @return CrossRelatedService
     */
    protected abstract function getCrossRelatedService(): CrossRelatedService;

    /**
     * @return string
     */
    protected abstract function getCatalogCrossModel(): string;
}
