<?php

namespace VmdCms\Modules\Products\Traits;

use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;

trait Availability
{
    use TaxonomyRelated;

    /**
     * @param $value
     * @throws NotCmsModelException
     */
    public function setAvailabilityAttribute($value)
    {
        $this->getCrossRelatedService()
            ->saveCrossRelation('availability',$value, $this->getTaxonomyCrossModel());
    }

    /**
     * @return mixed
     * @throws ModuleNotActiveException
     * @throws NotCmsCrossModelException
     */
    public function availability()
    {
        if(!class_exists(\VmdCms\Modules\Taxonomies\Models\Availability::class)){
            throw new ModuleNotActiveException();
        }

        return  $this->getCrossRelatedService()
            ->setCrossModel($this->getTaxonomyCrossModel())
            ->setRelatedModel(\VmdCms\Modules\Taxonomies\Models\Availability::class)
            ->hasThroughCrossModelWrapper('hasOneThrough');
    }
}
