<?php

namespace VmdCms\Modules\Products\Traits;

use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;

trait Catalog
{
    use CatalogRelated;

    /**
     * @param $value
     * @throws ModuleNotActiveException
     * @throws NotCmsModelException
     */
    public function setCatalogAttribute($value)
    {
        if(!class_exists(\VmdCms\Modules\Catalogs\Models\Catalog::class)){
            throw new ModuleNotActiveException();
        }

        $this->getCrossRelatedService()
            ->setRelatedModel(\VmdCms\Modules\Catalogs\Models\Catalog::class)
            ->saveCrossRelation('catalog',$value,$this->getCatalogCrossModel());
    }

    /**
     * @return mixed
     * @throws ModuleNotActiveException
     * @throws NotCmsCrossModelException
     */
    public function catalog()
    {
        if(!class_exists(\VmdCms\Modules\Catalogs\Models\Catalog::class)){
            throw new ModuleNotActiveException();
        }

        return  $this->getCrossRelatedService()
            ->setCrossModel($this->getCatalogCrossModel())
            ->setRelatedModel(\VmdCms\Modules\Catalogs\Models\Catalog::class)
            ->hasThroughCrossModelWrapper('hasManyThrough');
    }

}
