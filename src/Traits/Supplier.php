<?php

namespace VmdCms\Modules\Products\Traits;

use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;

trait Supplier
{
    use TaxonomyRelated;

    /**
     * @param $value
     * @throws NotCmsModelException
     */
    public function setSupplierAttribute($value)
    {
        $this->getCrossRelatedService()
            ->saveCrossRelation('supplier',$value, $this->getTaxonomyCrossModel());
    }

    /**
     * @return mixed
     * @throws ModuleNotActiveException
     * @throws NotCmsCrossModelException
     */
    public function supplier()
    {
        if(!class_exists(\VmdCms\Modules\Taxonomies\Models\Supplier::class)){
            throw new ModuleNotActiveException();
        }

        return  $this->getCrossRelatedService()
            ->setCrossModel($this->getTaxonomyCrossModel())
            ->setRelatedModel(\VmdCms\Modules\Taxonomies\Models\Supplier::class)
            ->hasThroughCrossModelWrapper('hasOneThrough');
    }
}
