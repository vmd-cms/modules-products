<?php

namespace VmdCms\Modules\Products\Traits;

use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;

trait Prices
{
    public function prices()
    {
        if(!class_exists(\VmdCms\Modules\Prices\Models\Price::class)){
            throw new ModuleNotActiveException();
        }
        return $this->hasMany(\VmdCms\Modules\Prices\Models\Price::class,'products_id','id')->orderBy('order');
    }

    public function price()
    {
        if(!class_exists(\VmdCms\Modules\Prices\Models\Price::class)){
            throw new ModuleNotActiveException();
        }
        return $this->hasOne(\VmdCms\Modules\Prices\Models\Price::class,'products_id','id');
    }

    public function priceGroups()
    {
        if(!class_exists(\VmdCms\Modules\Prices\Models\Price::class) || !class_exists(\VmdCms\Modules\Prices\Models\PriceGroup::class)){
            throw new ModuleNotActiveException();
        }
        return $this->hasManyThrough(\VmdCms\Modules\Prices\Models\PriceGroup::class, \VmdCms\Modules\Prices\Models\Price::class, 'products_id', 'id', 'id','price_groups_id')
            ->distinct()->with(['prices' => function($q) {
            $q->where('products_id', $this->id);
        }])->orderBy('order');
    }
}
