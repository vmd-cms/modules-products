<?php

namespace VmdCms\Modules\Products\Traits;

use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;

trait Brand
{
    use TaxonomyRelated;

    /**
     * @param $value
     * @throws NotCmsModelException
     */
    public function setBrandAttribute($value)
    {
        $this->getCrossRelatedService()
            ->saveCrossRelation('brand',$value, $this->getTaxonomyCrossModel());
    }

    /**
     * @return mixed
     * @throws ModuleNotActiveException
     * @throws NotCmsCrossModelException
     */
    public function brand()
    {
        if(!class_exists(\VmdCms\Modules\Taxonomies\Models\Brand::class)){
            throw new ModuleNotActiveException();
        }

        return  $this->getCrossRelatedService()
            ->setCrossModel($this->getTaxonomyCrossModel())
            ->setRelatedModel(\VmdCms\Modules\Taxonomies\Models\Brand::class)
            ->hasThroughCrossModelWrapper('hasOneThrough');
    }
}
