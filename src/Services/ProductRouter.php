<?php

namespace VmdCms\Modules\Products\Services;

class ProductRouter
{
    const PRODUCT_PREFIX = 'product.';

    const ROUTE_SET_FAVORITE = self::PRODUCT_PREFIX . 'set-favorite';
}
