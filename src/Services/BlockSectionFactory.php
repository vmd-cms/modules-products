<?php

namespace VmdCms\Modules\Products\Services;

use VmdCms\CoreCms\Services\BlockSectionFactoryAbstract;
use VmdCms\Modules\Products\Models\Components\Blocks\ProductInfoBlock;

class BlockSectionFactory extends BlockSectionFactoryAbstract
{
    protected static function getAssocKeySectionClasses() : array
    {
        return [
            ProductInfoBlock::getModelKey() => \App\Modules\Products\Sections\Components\Blocks\ProductInfoBlock::class,
        ];
    }
}
