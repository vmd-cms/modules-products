<?php

namespace VmdCms\Modules\Products\Services;

use VmdCms\CoreCms\Services\Enums;

class BlockEnum extends Enums
{
    const PRODUCT_INFO_BLOCK = 'product_info_block';
}
