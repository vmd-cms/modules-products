<?php

use VmdCms\Modules\Products\Models\Product as FirstModel;
use VmdCms\Modules\Products\Models\ProductCatalog as CrossModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!class_exists(\VmdCms\Modules\Catalogs\Models\Catalog::class)){
            return;
        }
        Schema::create(CrossModel::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer(FirstModel::table() . '_id')->unsigned();
            $table->integer(\VmdCms\Modules\Catalogs\Models\Catalog::table() . '_id')->unsigned();
            $table->timestamps();
            $table->unique([FirstModel::table() . '_id', \VmdCms\Modules\Catalogs\Models\Catalog::table() . '_id']);
        });
        Schema::table(CrossModel::table(), function (Blueprint $table){
            $table->foreign(FirstModel::table() . '_id', CrossModel::table() . '_' . FirstModel::table() . '_id' .'_fk')
                ->references(FirstModel::getPrimaryField())->on(FirstModel::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(\VmdCms\Modules\Catalogs\Models\Catalog::table() . '_id', CrossModel::table() . '_' . \VmdCms\Modules\Catalogs\Models\Catalog::table() . '_id' .'_fk')
                ->references(\VmdCms\Modules\Catalogs\Models\Catalog::getPrimaryField())->on(\VmdCms\Modules\Catalogs\Models\Catalog::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CrossModel::table());
    }
}
