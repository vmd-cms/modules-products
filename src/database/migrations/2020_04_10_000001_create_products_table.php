<?php

use VmdCms\Modules\Products\Models\Product as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments(model::getPrimaryField());
            $table->string('key',32)->nullable();
            $table->string('url',128)->nullable()->unique();
            $table->string('code',64)->nullable();
            $table->string('group_key',32)->nullable();
            $table->string('photo',255)->nullable();
            $table->boolean('active')->default(false);
            $table->text('data')->nullable();
            $table->string('import_source_id',128)->nullable();
            $table->boolean('deleted')->default(false);
            $table->integer('deleted_by')->unsigned()->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('deleted_by', model::table() . '_deleted_by_fk')->references('id')->on(\VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator::table())->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
