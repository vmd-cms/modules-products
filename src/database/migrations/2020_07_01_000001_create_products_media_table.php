<?php

use VmdCms\Modules\Products\Models\Media\ProductMedia as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->integer(model::getForeignField())->unsigned();
            $table->string('key',32)->nullable();
            $table->string('title',128)->nullable();
            $table->string('value',256)->nullable();
            $table->integer('order')->unsigned()->default(1);
            $table->boolean('active')->default(true);
            $table->string('import_source_id',128)->nullable();
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign(model::getForeignField(), model::table() . '_' . model::getForeignField().'_fk')
                ->references(model::getBaseModelClass()::getPrimaryField())->on(model::getBaseModelClass()::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
