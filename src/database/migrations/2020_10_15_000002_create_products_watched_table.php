<?php

use VmdCms\Modules\Products\Models\ProductWatched as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsWatchedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->string('session_id',128)->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('products_id')->unsigned();
            $table->timestamps();
            $table->unique(['products_id','user_id']);
            $table->unique(['products_id','session_id']);
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('products_id', model::table() . '_products_id_fk')
                ->references('id')->on('products')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        if(Schema::hasTable('users')){
            Schema::table(model::table(), function (Blueprint $table){
                $table->foreign('user_id', model::table() . '_user_id_fk')
                    ->references('id')->on('users')
                    ->onUpdate('CASCADE')->onDelete('CASCADE');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
