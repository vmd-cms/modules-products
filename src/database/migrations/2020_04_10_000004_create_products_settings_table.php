<?php

use VmdCms\Modules\Products\Models\ProductSettings as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->integer(model::getForeignField())->unsigned()->unique();
            $table->integer('amount')->unsigned()->default(0);
            $table->integer('items_in_box')->unsigned()->default(1);
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign(model::getForeignField(), model::table() . '_' . model::getForeignField().'_fk')
                ->references(model::getBaseModelClass()::getPrimaryField())->on(model::getBaseModelClass()::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
