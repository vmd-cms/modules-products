<?php

use VmdCms\Modules\Products\Models\ProductFavorite as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->integer('products_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->unique(['products_id','user_id']);
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('products_id', model::table() . '_products_id_fk')
                ->references('id')->on('products')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        if(Schema::hasTable('users')){
            Schema::table(model::table(), function (Blueprint $table){
                $table->foreign('user_id', model::table() . '_user_id_fk')
                    ->references('id')->on('users')
                    ->onUpdate('CASCADE')->onDelete('CASCADE');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
