<?php

namespace VmdCms\Modules\Products\database\seeds;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Initializers\AbstractModuleSeeder;
use App\Modules\Products\Sections\Product;
use App\Modules\Products\Sections\Components\ProductGroupBlock;
use App\Modules\Products\Sections\Components\ProductTranslate;

class ModuleSeeder extends AbstractModuleSeeder
{
    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            Product::class => CoreLang::get('products'),
            ProductGroupBlock::class => CoreLang::get('info_blocks') . ' ' . CoreLang::get('products'),
            ProductTranslate::class => CoreLang::get('translates') . ' ' . CoreLang::get('products'),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
            [
                "slug" => "shop_group",
                "title" => CoreLang::get('shop'),
                "is_group_title" => true,
                "order" => 2,
                "children" => [
                    [
                        "slug" => "products",
                        "title" => CoreLang::get('products'),
                        "icon" => "icon icon-order",
                        "section_class" => Product::class
                    ]
                ]
            ],
            [
                "slug" => "content_group",
                "title" => CoreLang::get('content'),
                "is_group_title" => true,
                "order" => 3,
                "children" => [
                    [
                        "slug" => "block_groups",
                        "title" => CoreLang::get('info_blocks'),
                        "icon" => "icon icon-copy",
                        "children" => [
                            [
                                "slug" => "product_blocks",
                                "title" => CoreLang::get('products'),
                                "section_class" => ProductGroupBlock::class,
                                "order" => 4,
                            ]
                        ]
                    ]
                ]
            ],
            [
                "slug" => "configs",
                "title" => CoreLang::get('configurations'),
                "is_group_title" => true,
                "order" => 5,
                "children" => [
                    [
                        "slug" => "translate_groups",
                        "title" => CoreLang::get('translates'),
                        "order" => 2,
                        "icon" => "icon icon-card",
                        "children" => [
                            [
                                "slug" => "product_translates",
                                "title" => CoreLang::get('products'),
                                "section_class" => ProductTranslate::class,
                                "order" => 5
                            ],
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function seedModelData()
    {

    }
}
