<?php

namespace VmdCms\Modules\Products\Collections;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\Modules\Products\Contracts\ProductDTOCollectionInterface;
use VmdCms\Modules\Products\Contracts\ProductDTOInterface;

class ProductDTOCollection extends CoreCollectionAbstract implements ProductDTOCollectionInterface,Arrayable
{
    /**
     * @param ProductDTOInterface $productDTO
     */
    public function append(ProductDTOInterface $productDTO)
    {
        $this->collection->add($productDTO);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function toArray()
    {
        return $this->collection->map(function (ProductDTOInterface $item){
            return $item->toArray();
        });
    }
}
