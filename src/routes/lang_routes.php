<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;
use VmdCms\Modules\Products\Services\ProductRouter;

return function (){
    Route::group([
        'namespace' => "App\\Modules\\Products\\Controllers",
    ],function (Router $router){
        $router->post('product/{id}/favorite', [
            'as'   => ProductRouter::ROUTE_SET_FAVORITE,
            'uses' => 'ProductController@setFavorite',
        ]);
    });
};
